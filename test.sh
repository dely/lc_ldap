#!/bin/bash

MSG=$(/usr/scripts/lc_ldap/test.py --mail | grep -v OK$)
TO="roots@crans.org"
#TO="samir@crans.org"
FROM="respbats@crans.org"
SUBJECT="Erreur dans les tests lc_ldap sur la base de test"
if [ -n "$MSG" ]; then
sendmail "$TO" << EOF
From: $FROM
To: $TO
Subject: $SUBJECT

$MSG
.
EOF
fi;
