
Utilisation de Sphinx
=====================

La doc est généré à partir des fichiers dans :file:`source/`, et est écrite en
`reStructuredText`_.

.. _reStructuredText: http://sphinx-doc.org/rest.html

Les plugins ``autodoc`` et ``coverage`` sont activés, ils permettent
respectivement de récupérer les docstrings depuis le code source, et de voir ce
qui n'est pas documenté (cf. :file:`build/coverage/python.txt`).

Pour générer la doc, il y a un :file:`Makefile`. Les règles les plus utiles :
 * :command:`make html` qui génère la documentation HTML dans :file:`build/html`,
 * :command:`make coverage` qui actualise la liste du code non documenté.

